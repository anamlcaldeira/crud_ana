<?php

use Illuminate\Database\Seeder;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Ana Maria',
            'email' => 'ana.maria@hotmail.com',
            'password' => bcrypt('123456')
        ]);
    }
}
