<?php

namespace App\Http\Requests\Painel;

use Illuminate\Foundation\Http\FormRequest;

class ClienteFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|min:3|max:100',
            'adress' => 'required|min:3|max:1000',
            'phone'      => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'O campo nome é de preenchimento obrigatório!',
            'adress' => 'O campo endereço é de preenchimento obrigatório!',
            'phone.numeric' => 'O campo telefone precisa ser preenchido apenas com números!',
            'number.required' => 'O campo telefone é de preenchimento obrigatório!',
        ];
    }
}
