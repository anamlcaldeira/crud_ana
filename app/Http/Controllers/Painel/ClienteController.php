<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Painel\Clientes;
use Illuminate\Validation\Validator;
use Symfony\Component\Console\Input\Input;
use App\Http\Requests\Painel\ClienteFormRequest;

class ClienteController extends Controller
{
    private $cliente;
    private $totalPage = 10;

    public function __construct(Clientes $cliente)
    {
        $this->cliente = $cliente;
    }

     public function index()
    {
        $title = 'Listagem dos clientes';

        $clientes = $this->cliente->paginate($this->totalPage);

        return view('painel.clientes.index-cliente', compact('clientes', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Cadastrar Novo Cliente';

        return view('painel.clientes.create-edit-cliente', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteFormRequest $request)
    {
        //dd($request->all());
        //dd($request->only(['name', 'number']));
        //dd($request->except(['_token', 'category']));
        //dd($request->input('name'));

        //Pega todos os dados que vem do formulário.
        $dataForm = $request->all();

        /*if( $dataForm['active'] == '')
            $dataForm['active'] = 0;
        else
            $dataForm['active'] = 1;*/

        //Validar os dados.
        //Uma opção: $this->validate($request, $this->product->rules);
        /*$messages = [
            'name.required' => 'O campo nome é de preenchimento obrigatório!',
            'number.numeric' => 'O campo número precisa ser preenchido apenas com números!',
            'number.required' => 'O campo número é de preenchimento obrigatório!',
        ];
        $validate = validator($dataForm, $this->product->rules, $messages);
        if( $validate->fails() ) {
            return redirect()
                        ->route('produtos.create')
                        ->withErrors($validate)
                        ->withInput();
        }*/


        //Faz o cadastro.
        $insert = $this->cliente->create($dataForm);

        if ( $insert )
            return redirect()->route('clientes.index');
        else
            return redirect()->route('clientes.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = $this->cliente->find($id);

        $title = "Cliente: {$cliente->name}";

        return view('painel.clientes.show-clientes', compact('cliente', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Recupera o produto pelo seu id.
        $cliente = $this->cliente->find($id);

        $title = "Editar Cliente: {$cliente->name}";

        return view('painel.clientes.create-edit-cliente', compact('title', 'cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteFormRequest $request, $id)
    {
        //Recupera todos os itens do formulario.
        $dataForm = $request->all();

        //Recupera o item para editar.
        $cliente = $this->cliente->find($id);

        //Verifica se o produto está ativado.
        //$dataForm['active'] = ( !isset($dataForm['active']) ) ? 0 : 1;

        //Altera os itens.
        $update = $cliente->update($dataForm);

        //Verifica se realmente editou.
        if( $update )
            return redirect()->route('clientes.index');
        else
            return redirect()->route('clientes.edit', $id)->with(['errors' => 'Falha ao editar.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = $this->cliente->find($id);

        $delete = $cliente->delete();

        if( $delete )
            return redirect()->route('clientes.index');
        else
            return redirect()->route('clientes.show', $id)->with(['errors' => 'Falha ao deletar!']);

    }

}
