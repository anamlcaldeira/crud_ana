<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Painel\Product;
use Illuminate\Validation\Validator;
use Symfony\Component\Console\Input\Input;
use App\Http\Requests\Painel\ProductFormRequest;

class ProdutoController extends Controller
{
    private $product;
    private $totalPage = 10;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

     public function index()
    {
        $title = 'Listagem dos produtos';

        $products = $this->product->paginate($this->totalPage);

        return view('painel.products.index', compact('products', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Cadastrar Novo Produto';

        $categorys = ['eletronicos', 'moveis', 'limpeza', 'banho'];

        return view('painel.products.create-edit', compact('title', 'categorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductFormRequest $request)
    {
        //dd($request->all());
        //dd($request->only(['name', 'number']));
        //dd($request->except(['_token', 'category']));
        //dd($request->input('name'));

        //Pega todos os dados que vem do formulário.
        $dataForm = $request->all();

        /*if( $dataForm['active'] == '')
            $dataForm['active'] = 0;
        else
            $dataForm['active'] = 1;*/

        $dataForm['active'] = ( !isset($dataForm['active']) ) ? 0 : 1;

        //Validar os dados.
        //Uma opção: $this->validate($request, $this->product->rules);
        /*$messages = [
            'name.required' => 'O campo nome é de preenchimento obrigatório!',
            'number.numeric' => 'O campo número precisa ser preenchido apenas com números!',
            'number.required' => 'O campo número é de preenchimento obrigatório!',
        ];
        $validate = validator($dataForm, $this->product->rules, $messages);
        if( $validate->fails() ) {
            return redirect()
                        ->route('produtos.create')
                        ->withErrors($validate)
                        ->withInput();
        }*/


        //Faz o cadastro.
        $insert = $this->product->create($dataForm);

        if ( $insert )
            return redirect()->route('produtos.index');
        else
            return redirect()->route('produtos.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->find($id);

        $title = "Produto: {$product->name}";

        return view('painel.products.show', compact('product', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Recupera o produto pelo seu id.
        $product = $this->product->find($id);

        $title = "Editar Produto: {$product->name}";

        $category = ['eletronicos'=>'eletronicos', 'moveis'=>'moveis', 'limpeza'=>'limpeza', 'banho'=>'banho'];

        return view('painel.products.create-edit', compact('title', 'category', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductFormRequest $request, $id)
    {
        //Recupera todos os itens do formulario.
        $dataForm = $request->all();

        //Recupera o item para editar.
        $product = $this->product->find($id);

        //Verifica se o produto está ativado.
        $dataForm['active'] = ( !isset($dataForm['active']) ) ? 0 : 1;

        //Altera os itens.
        $update = $product->update($dataForm);

        //Verifica se realmente editou.
        if( $update )
            return redirect()->route('produtos.index');
        else
            return redirect()->route('produtos.edit', $id)->with(['errors' => 'Falha ao editar.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->product->find($id);

        $delete = $product->delete();

        if( $delete )
            return redirect()->route('produtos.index');
        else
            return redirect()->route('produtos.show', $id)->with(['errors' => 'Falha ao deletar!']);

    }

    public function tests()
    {
        /*
        $prod = $this->product;
        $prod->name = 'Nome do Produto';
        $prod->number = 123456;
        $prod->active = true;
        $prod->name = 'eletronicos';
        $prod->description = 'Descrição do produto';
        $insert = $prod->save();

        if($insert)
            return 'Inserido com sucesso';
        else
            return 'Falha ao inserir';


        $insert = $this->product->create([
            'name'        => 'Nome do Produto',
            'number'      => 123456,
            'active'      => false,
            'category'    => 'eletronicos',
            'description' => 'Descrição do produto'
        ]);

        if($insert)
            return "Inserido com sucesso, ID: {$insert->id} ";
        else
            return 'Falha ao inserir';


        $prod = $this->product->find(5);
        $prod->name = 'Update 2';
        $prod->number = 123456;
        $update = $prod->save();

        if($update)
            return 'Alterado com sucesso!';
        else
            return 'Falha ao alterar.';


        $prod = $this->product->find(6);
        $update = $prod->update([
            'name'        => 'Update Teste',
            'number'      => 00000,
            'active'      => true,
        ]);

        if($update)
            return 'Alterado com sucesso!';
        else
            return 'Falha ao alterar.';
        */
        /*
        $update = $this->product
                        ->where('number', 0)
                        ->update([
                                    'name'        => 'Update Teste',
                                    'number'      => 123456,
                                    'active'      => false,
                                ]);

        if($update)
            return 'Alterado com sucesso 2!';
        else
            return 'Falha ao alterar.';
            */

        $delete = $this->product
                        ->where('id', 2)
                        ->delete();

        if($delete)
            return 'Deletado com sucesso 2.';
        else
            return 'Falha ao deletar 2.';
    }
}
