<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth');
       /*$this->middleware('auth')
                        ->only([
                            'contato',
                            'categoria'
                        ]);*/
       /* $this->middleware('auth')
                         ->except([
                             'index',
                             'contato'
                         ]);*/
    }

    public function index()
    {
        return view('welcome');
    }

    public function contato()
    {
        return view('Site.Contact.index');
    }

    public function categoria($id)
    {
        return "Listagem de posts da categoria: ($id)";
    }

    public function categoriaOp($id = 1)
    {
        return "Listagem de posts da categoria: ($id)";
    }
}
