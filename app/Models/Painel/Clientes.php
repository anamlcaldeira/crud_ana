<?php

namespace App\Models\Painel;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $fillable = [
        'name', 'adress', 'phone'
    ];

    //protected $guarded = ['admin'];
}
