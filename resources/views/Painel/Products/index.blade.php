@extends('adminlte::page')

@section('content')

<h1 class="title-pg">Listagem dos produtos</h1>
<br>
<a href= "{{route('produtos.create')}}">
    <button type="button" class="btn btn-info btn-add">
    <span class="glyphicon glyphicon-plus"></span> Cadastrar</button>
</a>

<table class="table table-striped">
    <tr>
        <th>Nome</th>
        <th>Descrição</th>
        <th width="100px">Ações</th>
    </tr>
    @foreach($products as $product)
    <tr>
        <td>{{$product->name}}</td>
        <td>{{$product->description}}</td>
        <td>
            <a href="{{route('produtos.edit', $product->id)}}" class="actions edit">
                <span class="glyphicon glyphicon-pencil"></span>
            </a>
            <a href="{{route('produtos.show', $product->id)}}" class="actions delete">
                <i class="fa fa-fw fa-search"></i>
            </a>
        </td>
    </tr>
    @endforeach
</table>

{!! $products->links() !!}

@endsection
