@extends('adminlte::page')

@section('content')

<h1 class="title-pg">
    <a href="{{route('clientes.index')}}"><span class="glyphicon glyphicon-triangle-left"></span></a>
    Cliente: <b>{{$cliente->name}}
</h1>
<p><b>Endereço:</b> {{$cliente->adress}}</p>
<p><b>Telefone:</b> {{$cliente->phone}}</p>

<br>

<hr>
@if( isset($errors) && count($errors) > 0)
    <div class="alert alert-danger">
        @foreach( $errors->all() as $error )
            <p>{{$error}}</p>
        @endforeach
    </div>
@endif

{!! Form::open(['route' => ['clientes.destroy', $cliente->id], 'method' => 'DELETE']) !!}
    {!! Form::submit("Deletar Cliente", ['class' => 'btn btn-danger']) !!}
{!! Form::close() !!}

@endsection
