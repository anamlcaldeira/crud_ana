@extends('adminlte::page')

@section('content')

<h1 class="title-pg">Listagem dos clientes</h1>
<br>
<a href= "{{route('clientes.create')}}">
    <button type="button" class="btn btn-info btn-add">
    <span class="glyphicon glyphicon-plus"></span> Cadastrar</button>
</a>

<table class="table table-striped">
    <tr>
        <th>Nome</th>
        <th width="100px">Ações</th>
    </tr>
    @foreach($clientes as $cliente)
    <tr>
        <td>{{$cliente->name}}</td>
        <td>
            <a href="{{route('clientes.edit', $cliente->id)}}" class="actions edit">
                <span class="glyphicon glyphicon-pencil"></span>
            </a>
            <a href="{{route('clientes.show', $cliente->id)}}" class="actions delete">
                <i class="fa fa-fw fa-search"></i>
            </a>
        </td>
    </tr>
    @endforeach
</table>

{!! $clientes->links() !!}

@endsection
